#!/usr/bin/env python

import argparse
import json
import os
import requests
from urllib3.connectionpool import xrange


def get_imgur_client_id():
    config_dir = os.path.expanduser("~") + '/.config'
    file_name = config_dir + '/image-downloader.json'
    if os.path.isfile(file_name):
        print('1')
        with open(file_name, 'r') as f:
            data = json.load(f)
    else:
        print('0')
        if not os.path.exists(config_dir):
            os.makedirs(config_dir)
        imgur_client_id = input("Please enter your imgur client id: ")
        # TODO: Check if client id is valid
        data = {"imgur_client_id": imgur_client_id}
        with open(file_name, 'w') as outfile:
            json.dump(data, outfile)
    return data['imgur_client_id']


clientId = get_imgur_client_id()
print(clientId)


def download_images(search, limit=5, size='small', type='png'):
    r = requests.get(
        "https://api.imgur.com/3/gallery/search/top/0/?q=" + search + "&limit=" + str(limit)
        + "&q_type=png&q_size_px=" + size,
        headers={"Authorization": "Client-ID " + clientId})
    json = r.json()

    num = 0
    for item in json['data']:
        if 'type' in item:
            num += 1
            print(num, item['title'])
            f = open(search + '_' + size + '_' + str(num) + '.' + type, 'wb')
            f.write(requests.get(item['link']).content)
            f.close()

            if num >= limit:
                break


parser = argparse.ArgumentParser()

parser.add_argument('search', help='Search string. Part of file name')
parser.add_argument('-t', '--type', metavar='',
                    choices=['jpg', 'png', 'gif', 'anigif'], default='png',
                    help='Get images for any file type, jpg | png | gif | anigif (animated gif). Default value is "png"')
parser.add_argument('-s', '--size', metavar='',
                    choices=['small', 'med', 'big', 'lng', 'huge'], default='med',
                    help='Size ranges, small (500 pixels square or less) | med (500 to 2,000 pixels square) | big (2,'
                         '000 to 5,000 pixels square) | lrg (5,000 to 10,000 pixels square) | huge (10,000 square '
                         'pixels and above). Default value is "med"')
parser.add_argument('-l', '--limit', metavar='',
                    type=int, choices=xrange(1, 60 + 1), default=5,
                    help='Limit of number of downloaded images. Should be in range 1 - 60. Default value is 5')

args = parser.parse_args()

download_images(args.search, limit=args.limit, size=args.size, type=args.type)
